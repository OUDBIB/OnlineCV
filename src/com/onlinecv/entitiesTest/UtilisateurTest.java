package com.onlinecv.entitiesTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.jupiter.api.Test;

import com.onlinecv.entities.Authentification;
import com.onlinecv.entities.Utilisateur;

public class UtilisateurTest {

	@Test
	public void testConstruire() {
		Utilisateur utilisateur = new Utilisateur();
		assertNotNull(utilisateur);
	}

	@Test
	public void testGetCode() {
		Utilisateur utilisateur = new Utilisateur();
		String nom = utilisateur.getNom();
		assertNull(nom);
	}

	@Test
	public void testSetNom() {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setNom("Toto");
		String nom = utilisateur.getNom();
		assertEquals("Toto", nom);
	}

	@Test
	public void testGetAuthentificationEmail() {
		Utilisateur utilisateur = new Utilisateur();
		Authentification auth= new Authentification();
		utilisateur.setAuthentification(auth);
		String email = auth.getEmail();
		assertNull(email);
	}

	@Test
	public void testSetAuthentificationEmail() {
		Utilisateur utilisateur = new Utilisateur();
		Authentification auth= new Authentification();
		utilisateur.setAuthentification(auth);
		auth.setEmail("Toto@toto.fr");
		String email = utilisateur.getAuthentification().getEmail();
		assertEquals("Toto@toto.fr", email);
	}
	
}
