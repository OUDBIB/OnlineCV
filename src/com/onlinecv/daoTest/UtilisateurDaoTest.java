package com.onlinecv.daoTest;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.onlinecv.config.AppConfig;
import com.onlinecv.dao.IUtilisateurJpaRepository;
import com.onlinecv.entities.Authentification;
import com.onlinecv.entities.Utilisateur;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })

public class UtilisateurDaoTest extends SpringTest{

	
	@Autowired
	private IUtilisateurJpaRepository clientRepo;
	
	@Test
	public void testfindAllOk() {
		assertTrue("Liste vide", clientRepo.findAll().isEmpty() == false);

	}

	@Test
	public void testfindbyId() {
		assertFalse("Objet non existant", clientRepo.findById((long) 1) == null);

	}

	@Test
	public void testsave() {
		Utilisateur utilisateur = new Utilisateur();
		Authentification authentification = new Authentification();
		utilisateur.setAuthentification(authentification);
		authentification.setEmail("pb@free.fr");
		authentification.setMotDePasse("123");
		
		utilisateur.setAge("999999");
		utilisateur.setNom("Kiwis");
		utilisateur.setPrenom("titi");
		

		clientRepo.save(utilisateur);
	}

}