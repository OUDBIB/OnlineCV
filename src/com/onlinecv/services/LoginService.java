package com.onlinecv.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;

import com.onlinecv.config.Principal;
import com.onlinecv.dao.IAdminJpaRepository;
import com.onlinecv.dao.IUtilisateurJpaRepository;
import com.onlinecv.entities.Admin;
import com.onlinecv.entities.Utilisateur;

@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private IUtilisateurJpaRepository clientRepo;

    @Autowired
    private IAdminJpaRepository adminRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	Utilisateur utilisateur = clientRepo.findByAuthentificationEmail(username);
	if (null == utilisateur) {
	    Admin admin = adminRepo.findByAuthentificationEmail(username);
	    if (null == admin) {
		throw new UsernameNotFoundException("No user found with username: " + username);
	    }
	    return new Principal(admin);
	}
	return new Principal(utilisateur);
    }
}
