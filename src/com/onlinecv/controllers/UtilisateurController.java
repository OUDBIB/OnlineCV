package com.onlinecv.controllers;

import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.onlinecv.dao.IAuthentificationJpaRepository;
import com.onlinecv.dao.IUtilisateurJpaRepository;
import com.onlinecv.entities.Authentification;
import com.onlinecv.entities.Utilisateur;
import com.onlinecv.entities.ERole;

@Controller
@RequestMapping("/client")
public class UtilisateurController {

    @Autowired
    private IUtilisateurJpaRepository clientRepo;

    @Autowired
    private IAuthentificationJpaRepository authentificationRepo;

    

//    @RequestMapping("/goToAccueil")
//    public String gotoAccueil(@RequestParam(value = "logout", required = false) Boolean logout, Model model) {
//	List<Sport> listeSports = sportRepo.findAll();
//	model.addAttribute("listeSports", listeSports);
//	return "accueil";
//    }

    @Secured("ROLE_CLIENT")
    @RequestMapping("/goToMenuClient")
    public String gotoMenuClient(Model model) {
	return "menuClient";
    }

    @GetMapping("/goToCreer")
    public String goToCreer(Model model) {
	model.addAttribute("client", new Utilisateur());
	return "inscription";
    }

    @GetMapping("/goToConsulterCompte")
    public String goToConsulterCompte(Model model) {
	Utilisateur utilisateur = clientRepo.getOne(AuthHelper.getClient().getId());
	model.addAttribute("client", utilisateur);
	return "inscription";
    }

    @PostMapping("/creer")
    public String creer(@Valid @ModelAttribute(value = "client") Utilisateur utilisateur, BindingResult result, Model model) {

	if (!result.hasErrors()) {
	    String email = utilisateur.getAuthentification().getEmail();
	    Authentification other = null;
	    Utilisateur otherClient = null;
	    Long id = utilisateur.getId();

	    if (id == null) { // creation d'un compte
		other = authentificationRepo.findByEmail(email);
	    } else { // modification d'un compte
		otherClient = clientRepo.findClientByIdNotAndAuthentificationEmail(id, email);
	    }

	    if (other != null) {
		result.rejectValue("authentification.email", "error.authentification.email.doublon");
	    }
	}
	if (!result.hasErrors()) {
	    encodePassword(utilisateur.getAuthentification());
	    utilisateur.getAuthentification().setRole(ERole.ROLE_CLIENT);
	    clientRepo.save(utilisateur);
	    model.addAttribute("client", new Utilisateur());
	    if (AuthHelper.isAuthenticated())
		return "menuClient";
	    else
		return "accueil";
	} else {
	    return "inscription";
	}
    }

//    @Secured("ROLE_CLIENT")
//    @RequestMapping("/goToRencontresAvenir")
//    public String goToRencontresAvenir(Model model) {
//	Date now = new Date();
//	List<Rencontre> listeRencontres = rencontreRepo.findNextEvents(now);
//	model.addAttribute("listeRencontres", listeRencontres);
//	return "rencontresAVenir";
//    }

//    @Secured("ROLE_CLIENT")
//    @GetMapping("/goToPari/{id_rencontre}")
//    public String goToPari(@PathVariable(value = "id_rencontre", required = true) Long id_rencontre, Model model) {
//	Pari pari = new Pari();
//	pari.setRencontre(rencontreRepo.getOne(id_rencontre));
//	pari.setClient(clientRepo.getOne(AuthHelper.getClient().getId()));
//	model.addAttribute("pari", pari);
//	return "pari";
//    }

//    @Secured("ROLE_CLIENT")
//    @PostMapping("/pari")
//    public String Pari(@Valid @ModelAttribute(value = "pari") Pari pari, BindingResult result, Model model) {
//	if (pari.getSomme() > pari.getClient().getMontantMax()) {
//	    result.rejectValue("somme", "error.pari.somme.excessive");
//	}
//	if (!result.hasErrors()) {
//	    pariRepo.save(pari);
//	    model.addAttribute("pari", new Pari());
//	    List<Pari> listePariByClient = pariRepo.findByClientId(AuthHelper.getClient().getId());
//	    model.addAttribute("listePariByClient", listePariByClient);
//	    return "rencontresPariees";
//	} else {
//	    return "pari";
//	}
//    }

//    @Secured("ROLE_CLIENT")
//    @RequestMapping("/goToRencontresPariees")
//    public String goToRencontresPariees(@ModelAttribute(value = "client") Utilisateur client, Model model) {
//	List<Pari> listePariByClient = pariRepo.findByClientId(AuthHelper.getClient().getId());
//	model.addAttribute("listePariByClient", listePariByClient);
//	return "rencontresPariees";
//    }

    private static void encodePassword(Authentification authentification) {
	String rawPassword = authentification.getMotDePasse();
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	String encodedPassword = encoder.encode(rawPassword);
	authentification.setMotDePasse(encodedPassword);
    }
}
