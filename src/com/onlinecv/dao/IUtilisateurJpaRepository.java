package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.Utilisateur;

public interface IUtilisateurJpaRepository extends JpaRepository<Utilisateur, Long> {

    public Utilisateur findByAuthentificationEmail(String email);

    @Query("select c from Utilisateur c where c.id<>:id and c.authentification.email=:email")
    public Utilisateur findClientByIdNotAndAuthentificationEmail(@Param("id") Long id, @Param("email") String email);

}
