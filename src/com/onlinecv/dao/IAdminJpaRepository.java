package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Admin;

public interface IAdminJpaRepository extends JpaRepository<Admin, Long> {

    public Admin findByAuthentificationEmail(String username);

}
